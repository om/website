#!/bin/bash
docker run --rm -v "$(pwd):/srv/jekyll" jekyll/jekyll jekyll build
docker build -t docker.data.coop/cryptoaarhus-website .
