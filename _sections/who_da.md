# Hvad er det

---
Cryptoaarhus er en stribe uformelle og hyggelige sammenkomster som foregår den anden lørdag i måneden. Vi er en flok privacy elskende nørder der sidder klar til at hjælpe dig i gang med at bruge nogle af de populære og anerkendte privacy- og sikkerhedsværktøjer – både på din smartphone og på din computer.

Events vil blive annonceret på Twitter via profilen [@cryptoaarhus][twitter], her kan du også komme i kontakt med stamdeltagerne hvis du har spørgsmål.

Så kom ned og sig hej, drik en kop lækker kaffe, og få en sludder om privatliv, sikkerhed og dataetik på internettet.

Hvis du bor tættere på København, er [Cryptohagen][cryptohagen] måske mere interessant for dig.


[cryptohagen]:https://cryptohagen.dk/
[twitter]:https://twitter.com/cryptoaarhus
