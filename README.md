# cryptoaarhus.dk

[![Build Status](https://drone.data.coop/api/badges/cryptoaarhus.dk/website/status.svg)](https://drone.data.coop/cryptoaarhus.dk/website)

This repo holds the homepage of https://cryptoaarhus.dk/

## Running locally for development:

 - Install Docker and docker-compose
 -  Run ``docker-compose up``. THis starts a local web server on port 4000 with LiveReload enabled, enabling you to tweak and play without having to F5 your browser.

If you have changes to `Gemfile`, run ``docker-compose run jekyll bundle update``.

## Publishing to cryptoaarhus.dk

If you have push access to `master` at https://git.data.coop/cryptoaarhus.dk/website, doing so wil trigger a build and release.
